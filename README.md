
# Java-maven-Junit-demo


This project is intended to be a How-to guide to get started with JUnit for java/maven based projects. 

What does this project contains?
- Sample unit test uploading test results.


## Getting Started

[Fork](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork) this repository using the gitlab mechanism or you can clone the entire repository as follows:

```bash
git clone git@gitlab.com:ayd2_1s_2020/clase5-maven-junit.git
```
```bash
git clone https://gitlab.com/ayd2_1s_2020/clase5-maven-junit.git
```

### Tools

The main tools used in this pipeline are:

- Java 8: General purpose, multiplatform programming language. 
- JUnit 4: Unit testing framework form the Java programming language.
- Maven: Build automation tool for java projects.


Those tools will be applied in the areas:
- Test Automation

Apache Maven
============

[![Apache License, Version 2.0, January 2004](https://img.shields.io/github/license/apache/maven.svg?label=License)][license]
[![Maven Central](https://img.shields.io/maven-central/v/org.apache.maven/apache-maven.svg?label=Maven%20Central)](https://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22org.apache.maven%22%20AND%20a%3A%22apache-maven%22)
[![Jenkins Status](https://img.shields.io/jenkins/s/https/builds.apache.org/job/maven-box/job/maven/job/master.svg?style=flat-square)][build]
[![Jenkins tests](https://img.shields.io/jenkins/t/https/builds.apache.org/job/maven-box/job/maven/job/master.svg?style=flat-square)][test-results]


Apache Maven is a software project management and comprehension tool. Based on
the concept of a project object model (POM), Maven can manage a project's
build, reporting and documentation from a central piece of information.

If you think you have found a bug, please file an issue in the [Maven Issue Tracker](https://issues.apache.org/jira/browse/MNG).

Documentation
-------------

More information can be found on [Apache Maven Homepage][maven-home].
Questions related to the usage of Maven should be posted on
the [Maven User List][users-list].


Where can I get the latest release?
-----------------------------------
You can download release source from our [download page][maven-download].

Contributing
------------

If you are interested in the development of Maven, please consult the 
documentation first and afterwards you are welcome to join the developers 
mailing list to ask question or discuss new ideas / features / bugs etc.

Take a look into the [contribution guidelines](CONTRIBUTING.md).

License
-------
This code is under the [Apache License, Version 2.0, January 2004][license].

See the `NOTICE` file for required notices and attributions.

Donations
---------
You like Apache Maven? Then [donate back to the ASF](https://www.apache.org/foundation/contributing.html) to support the development.

Quick Build
-------
If you want to bootstrap Maven, you'll need:
- Java 1.8+
- Maven 3.0.5 or later
- Run Maven, specifying a location into which the completed Maven distro should be installed:
```
mvn -DdistributionTargetDir="$HOME/app/maven/apache-maven-3.7.x-SNAPSHOT" clean package
```


[home]: https://maven.apache.org/
[license]: https://www.apache.org/licenses/LICENSE-2.0
[build]: https://builds.apache.org/job/maven-box/job/maven/job/master/
[test-results]: https://builds.apache.org/job/maven-box/job/maven/job/master/lastCompletedBuild/testReport/
[build-status]: https://img.shields.io/jenkins/s/https/builds.apache.org/job/maven-box/job/maven/job/master.svg?style=flat-square
[build-tests]: https://img.shields.io/jenkins/t/https/builds.apache.org/job/maven-box/job/maven/job/master.svg?style=flat-square
[maven-home]: https://maven.apache.org/
[maven-download]: https://maven.apache.org/download.cgi
[users-list]: https://maven.apache.org/mailing-lists.html
[dev-ml-list]: https://www.mail-archive.com/dev@maven.apache.org/
[code-style]: http://maven.apache.org/developers/conventions/code.html
[core-it]: https://maven.apache.org/core-its/core-it-suite/
[building-maven]: https://maven.apache.org/guides/development/guide-building-maven.html
[cla]: https://www.apache.org/licenses/#clas


## Compile project (build)

In this stage we compile the project source code and pass target folder to the next stage

### Test and Import results (test stage)

In this stage we execute the project test cases. After running the SoapUI tests, this stage loads the results and creates a Test Execution on the Jira Server that you define in the CI/CD variables using the Rest API.

### Static code analysis and test coverage (verify stage)

In this stage we execute the static code analysis using sonarqube plugin, and calculate test coverage using JaCoCo plugin. 



## Author

* **Dodany Girón**      - dodanygiron@gmail.com 
Análisis y DIseño de sistemas 2
USAC


## Tarea  4
//BEGIN
Entrega para el día Sábado 28/03/2020
1. Clonar el repo (Gettin Started) 

2. Inicilizar su repo  (opcional) mvn install 

3. Crear Rama /feature/t4_Carné -->   /feature/t4_2020111111

4. Crear una clase dentro del paquete main/java/com/usac de la siguiente manera:
   T4_Carné.java --> T4_20201111.java
    La clase debe contener : 
    - Constructor
    - Método que devuelve el perimetro de un circulo (2 parámetros)
    - Método que devuelva el área circulo (2 parámetros)
    - Método con la serie fibonacci  (1 parámetro)
    - Método que devuelve el factorial de un número (1 parámetro)
    - Método que devuelva true/false si una palabra es palindroma (1 parámetro(String))

5. Crear una clase dentro del paquete test/java/com/usac  de la siguiente manera:
    T4_CarnéTest.java --> T4_2020111Test.java
    1 Testcase por método creado en la clase t4_Carné.java
     (no olviden investigar sobre las anotaciones de JUnit @Test   @Before @BeforClass  @After  @AfterClass @Test(expected ))
        http://java-white-box.blogspot.com/2014/06/junitanotacionesAnnotationJUnit.html 

6. Verificación (opcional) : mvn test 

7. Subir los cambios
    - git add , git commit -m "Tarea-4 _nombre y _Carné" 
    - git push

8. Realizar Merge Request 

9. Éxitos a todos (Recuerden no salir de casa)
//END