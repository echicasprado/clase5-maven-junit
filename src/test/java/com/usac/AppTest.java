package com.usac;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testConcat() {
        String s="Hola";
        String s2 = s.concat ( " Mundo ");

        assertTrue( s2.equals("Hola Mundo "));
    }
}
