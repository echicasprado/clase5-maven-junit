package com.usac;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * HigherParametroTest
 */
@RunWith(value = Parameterized.class)
public class HigherParametroTest {

    private int a,b,exp;

    @Parameters
    public static Iterable<Object[]> getData(){
        List <Object[]> obj= new ArrayList<>();
        
        obj.add(new Object[]{ 3,1,4 });
        obj.add(new Object[]{ 25,5,30 });
        obj.add(new Object[]{ 25,25,50 }) ;

        return obj;        
    }
    
    public HigherParametroTest(int a, int b, int exp) {
        this.a = a;

        this.b = b;
        this.exp = exp;
    }
    
    @Test
    public void testAdd(){
        Higher h = new Higher();
        int result = h.add(a,b);

        assertEquals( exp, result);
    }
    

}