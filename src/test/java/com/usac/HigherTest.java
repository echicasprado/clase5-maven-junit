package com.usac;

import static org.junit.Assert.assertEquals;

import org.junit.*;


import junit.framework.TestCase;
import org.junit.rules.TestWatcher;


/**
 * HigherTest
 */
public class HigherTest extends TestCase {

    static Higher h;


    @BeforeClass
    public static void before() {
        System.out.println("Before");
        h = new Higher();
    }
 
    @Test
    public void testHigher() {
        h = new Higher();
        System.out.println("testHigher");

        assertEquals( 25,  h.higherxy(25, 5));
        
        assertEquals(10, h.higherxy(10, 5));
        
        assertTrue( h.add(2, 2) == (2 + 2));
        
        assertEquals( 4, h.add(2, 2));

        assertEquals("la prueba no funciono", 25,  h.higherxy(25, 5));

        

    }



    @Test
    public void testSum() {
        h = new Higher();
        System.out.println("testSum");
        assertEquals(5,  h.add(3, 2));
    }

    @Test
    public void testAnsSum() {
        h = new Higher();
        System.out.println("testAnsSum");
        h.add(3, 2);
        assertEquals(5, h.ans());
    }

    @Test
    public void testDiv(){
        h = new Higher();
        h.div(5,2);
    }

    @Test(expected = ArithmeticException.class)
    public void testDivPorCero(){
        h = new Higher();
        h.div(5,2);
    }


    @After
    public void after(){
        System.out.println("after()");
        h = new Higher();
        h.clear();

    }

}