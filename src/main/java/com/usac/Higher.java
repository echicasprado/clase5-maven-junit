package com.usac;

/**
 * Higher
 */
public class Higher {

    private int ans;

    public Higher (){
        ans=0;        
    }
    
    /**
     * 
     * @param x
     * @param y
     * @return the higher of the two values
     */
    public int higherxy(int x, int y) {
        if (x > y) {
            return x;
        } else {
            return y;
        }
    }

     public int add(int x , int y ){
        ans = x+y;
        return ans;
     }

     public int sub(int x , int y){
         ans = x -y;
         return ans;
     }

    public int add( int v){
        ans += v;
        return ans;
    }

    public int sub(int v ){
        ans -= v;
        return ans;
    }

    public int ans(){
        return ans;
    }

    public int clear(){
        return 0;
    }

    public int div(int x, int y){
        if (y==0 ){
            throw new ArithmeticException("No puede ser divido por cero");
        }
        ans = x/y;
        return ans;
    }

}